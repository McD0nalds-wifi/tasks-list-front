import { createSelector } from 'reselect'

import { appApi } from '@/shared/api'

import { Task } from '../types'

interface ITasksResponse {
    hasMore: boolean
    tasks: Array<Task>
}

export const tasksApi = appApi.injectEndpoints({
    endpoints: (build) => ({
        getTasks: build.query<ITasksResponse, void>({
            query: () => ({
                method: 'GET',
                url: 'tasks',
            }),
        }),
    }),
})

const selectTasksResult = () => tasksApi.endpoints.getTasks.select()

const selectTasksCount = createSelector(selectTasksResult(), ({ data }) => data?.tasks.length ?? 0)

export const tasksSelectors = {
    selectTasksCount,
}

export const { useGetTasksQuery } = tasksApi
