/**
 * @example getDeclOfNum(5, ['минута', 'минуты', 'минут']); // вернёт — 1 минут
 *
 * @param {Number} n: 5
 * @param {Array} wordForms: ['минута', 'минуты', 'минут']
 * @returns {String} "5 минут"
 */
export const getDeclOfNum = (n: number, wordForms: Array<string>) => {
    n = Math.abs(n) % 100
    const n1 = n % 10

    if (n > 10 && n < 20) {
        return wordForms[2]
    }

    if (n1 > 1 && n1 < 5) {
        return wordForms[1]
    }

    if (n1 == 1) {
        return wordForms[0]
    }

    return wordForms[2]
}
