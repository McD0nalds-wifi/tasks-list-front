export interface IUser {
    avatar: string | null
    email: string
    firstName: string
    id: string
    lastName: string
}
