import type { Meta, StoryObj } from '@storybook/react'

import { RadialBar } from './'

const meta: Meta<typeof RadialBar> = {
    component: RadialBar,
}

export default meta
type Story = StoryObj<typeof RadialBar>

export const Playground: Story = {
    args: {
        backgroundColor: 'gray5',
        fontColor: 'white',
        fontSize: 20,
        percentage: 40,
        strokeColor: 'orange',
        strokeColorType: 'color',
        strokeWidth: 10,
        width: 200,
    },
}
