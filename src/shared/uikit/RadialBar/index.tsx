import { COLORS, ColorsType } from '@/shared/constants'

import style from './index.module.scss'

type IRadialBarProps = {
    backgroundColor: ColorsType
    fontColor: ColorsType
    fontSize: number
    percentage: number
    strokeWidth: number
    width: number
} & ({ strokeColor: ColorsType; strokeColorType: 'color' } | { strokeColorType: 'image'; strokeImage: string })

export const RadialBar = ({
    backgroundColor,
    fontColor,
    fontSize,
    percentage,
    strokeWidth,
    width,
    ...otherProps
}: IRadialBarProps) => {
    const radius = width / 2 - strokeWidth / 2
    const dashArray = radius * Math.PI * 2
    const dashOffset = dashArray - (dashArray * percentage) / 100

    return (
        <svg height={width} viewBox={`0 0 ${width}`} width={width}>
            {otherProps.strokeColorType === 'image' ? (
                <defs>
                    <pattern height={width} id={'image'} patternUnits={'userSpaceOnUse'} width={width} x={'0'} y={'0'}>
                        <image height={width} width={width} x={'0'} xlinkHref={otherProps.strokeImage} y={'0'}></image>
                    </pattern>
                </defs>
            ) : null}

            <circle
                cx={width / 2}
                cy={width / 2}
                fill={COLORS[backgroundColor]}
                r={radius}
                stroke={COLORS[backgroundColor]}
                strokeWidth={strokeWidth}
            />

            <circle
                className={style.circleProgress}
                cx={width / 2}
                cy={width / 2}
                r={radius}
                stroke={otherProps.strokeColorType === 'image' ? 'url(#image)' : otherProps.strokeColor}
                strokeWidth={strokeWidth}
                style={{
                    strokeDasharray: dashArray,
                    strokeDashoffset: dashOffset,
                }}
                transform={`rotate(-90 ${width / 2} ${width / 2})`}
            />

            <text
                className={style.circleText}
                dy={'0.3em'}
                fill={COLORS[fontColor]}
                style={{ fontSize }}
                textAnchor={'middle'}
                x={'50%'}
                y={'50%'}
            >
                {percentage}%
            </text>
        </svg>
    )
}
