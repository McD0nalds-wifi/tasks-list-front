'use client'

import { ChangeEvent } from 'react'

// import { SearchIcon } from '@/shared/uikit/icons'

import style from './index.module.scss'

interface ISearchInputProps {
    onChange?: (event: ChangeEvent<HTMLInputElement>) => void
    placeholder?: string
    value?: string
}

export const SearchInput = ({ onChange, placeholder, value }: ISearchInputProps) => {
    return (
        <label className={style.wrapper}>
            {/*<SearchIcon color={'primary3'} cursor={'inherit'} height={'22px'} width={'22px'} />*/}

            <input
                className={style.input}
                onChange={onChange}
                placeholder={placeholder}
                type={'search'}
                value={value}
            />
        </label>
    )
}
