'use client'

import React, { ReactNode } from 'react'

import { clsx } from 'clsx'

import { Typography } from '@/shared/uikit'

import style from './index.module.scss'

interface IButtonProps {
    children?: ReactNode
    color?: 'primary' | 'secondary'
    disabled?: boolean
    endIcon?: ReactNode
    onClick?: () => void
    size?: 'small' | 'medium' | 'large'
    startIcon?: ReactNode
}

const TYPOGRAPHY_VARIANT_BY_BUTTON_SIZE = {
    large: 'h3',
    medium: 'body1',
    small: 'body2',
} as const

const TYPOGRAPHY_WEIGHT_BY_BUTTON_SIZE = {
    large: 'medium',
    medium: 'medium',
    small: 'regular',
} as const

export const Button = ({
    children,
    color = 'primary',
    disabled = false,
    endIcon,
    onClick,
    size = 'medium',
    startIcon,
}: IButtonProps) => {
    return (
        <button
            className={clsx(style.button, style[`button__${color}`], style[`button__${size}`])}
            disabled={disabled}
            onClick={onClick}
        >
            {startIcon}

            <Typography
                color={'inherit'}
                component={'span'}
                variant={TYPOGRAPHY_VARIANT_BY_BUTTON_SIZE[size]}
                weight={TYPOGRAPHY_WEIGHT_BY_BUTTON_SIZE[size]}
            >
                {children}
            </Typography>

            {endIcon}
        </button>
    )
}
