import type { Meta, StoryObj } from '@storybook/react'

import { Button } from './'

const meta: Meta<typeof Button> = {
    component: Button,
    parameters: {
        backgrounds: {
            default: 'uikit',
            values: [{ name: 'uikit', value: '#808080' }],
        },
    },
}

export default meta
type Story = StoryObj<typeof Button>

export const Playground: Story = {
    args: {
        children: 'Button',
        color: 'primary',
        size: 'medium',
    },
}
