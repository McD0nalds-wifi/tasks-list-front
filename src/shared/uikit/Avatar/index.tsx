import style from './index.module.scss'

interface IAvatarProps {
    size?: number | string
}

export const Avatar = ({ size }: IAvatarProps) => {
    return <div className={style.avatar} style={{ height: size, width: size }} />
}
