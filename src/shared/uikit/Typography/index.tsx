'use client'

import { ReactNode } from 'react'

import { clsx } from 'clsx'

import { ColorsType } from '@/shared/constants'

import style from './index.module.scss'

interface ITypographyProps {
    align?: 'left' | 'center' | 'right'
    children?: ReactNode
    color?: ColorsType
    component?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'div' | 'span' | 'a'
    cursor?: 'default' | 'pointer' | 'inherit'
    variant?: 'h1' | 'h2' | 'h3' | 'body1' | 'body2' | 'caption'
    weight?: 'regular' | 'medium' | 'bold'
}

export const Typography = ({
    align = 'left',
    children,
    color = 'black',
    component = 'div',
    cursor = 'inherit',
    variant = 'body1',
    weight = 'regular',
}: ITypographyProps) => {
    const Tag = component

    return (
        <Tag
            className={clsx(style[variant], style[weight])}
            style={{
                color,
                cursor,
                textAlign: align,
            }}
        >
            {children}
        </Tag>
    )
}
