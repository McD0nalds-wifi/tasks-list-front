import { appApi, getFetchBaseQueryError } from '@/shared/api'

import { store } from './index'
export { store, appApi, getFetchBaseQueryError }

export type RootStateType = ReturnType<typeof store.getState>

export type AppDispatchType = typeof store.dispatch
