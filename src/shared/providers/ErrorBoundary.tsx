'use client'

import { ReactNode } from 'react'

import {
    ErrorBoundary as ErrorBoundaryBase,
    ErrorBoundaryPropsWithComponent,
    FallbackProps,
} from 'react-error-boundary'

interface ErrorBoundaryProps {
    FallBackComponent?: ErrorBoundaryPropsWithComponent['FallbackComponent']
    children: ReactNode
}

// TODO Add styles for error
export const ErrorFallback = ({ error, resetErrorBoundary }: FallbackProps) => (
    <div>
        <p>Something went wrong:</p>
        <pre>{error.message}</pre>
        <button onClick={resetErrorBoundary}>Try again</button>
    </div>
)

export const ErrorBoundary = ({ FallBackComponent = ErrorFallback, children }: ErrorBoundaryProps) => (
    <ErrorBoundaryBase FallbackComponent={FallBackComponent}>{children}</ErrorBoundaryBase>
)
