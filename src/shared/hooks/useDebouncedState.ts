import { useEffect, useState } from 'react'

interface IUseDebouncedStateArgs<T> {
    defaultValue: T
    delay: number
}

export const useDebouncedState = <T>({ defaultValue, delay }: IUseDebouncedStateArgs<T>) => {
    const [state, setState] = useState(defaultValue)
    const [debouncedValue, setDebouncedValue] = useState(defaultValue)

    useEffect(() => {
        const handler = setTimeout(() => {
            setDebouncedValue(state)
        }, delay)

        return () => {
            clearTimeout(handler)
        }
    }, [state, delay])

    return { debouncedValue, setState, state }
}
