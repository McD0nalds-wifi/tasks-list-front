import { useEffect, useRef } from 'react'

export const useEventListener = <K extends keyof WindowEventMap>(
    eventType: K,
    callback: (event: WindowEventMap[K]) => void,
    element = window,
) => {
    const callbackRef = useRef(callback)

    useEffect(() => {
        callbackRef.current = callback
    }, [callback])

    useEffect(() => {
        if (element == null) {
            return
        }
        const handler = (event: WindowEventMap[K]) => callbackRef.current(event)

        element.addEventListener(eventType, handler)

        return () => {
            element.removeEventListener(eventType, handler)
        }
    }, [eventType, element])
}
