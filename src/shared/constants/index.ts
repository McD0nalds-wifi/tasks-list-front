export const TOKEN_NAME = 'token'

export const COLORS = {
    aqua: '#91e0fe',
    black: '#000000',
    blue: '#6373f7',
    gray1: '#d2d2d2',
    gray2: '#bcbcbc',
    gray3: '#808080',
    gray4: '#333333',
    gray5: '#1d1d1d',
    gray6: '#0a0a0a',
    green: '#76ef83',
    inherit: 'inherit',
    orange: '#ffc83f',
    pink: '#febd93',
    red: '#ff6b6b',
    white: '#ffffff',
} as const

export type ColorsType = keyof typeof COLORS
