'use client'

import { useState } from 'react'

import { MobileDayPicker } from '@/features/select-date'
import { Button, RadialBar, Typography } from '@/shared/uikit'
import { CalendarIcon } from '@/shared/uikit/icons'

import style from './page.module.scss'
import gradientImage from '../../public/gradient.png'

const Home = () => {
    const [date, setDate] = useState(new Date())

    console.log(date)

    return (
        <div className={style.wrapper}>
            <div className={style.tasksInfo}>
                <RadialBar
                    backgroundColor={'gray5'}
                    fontColor={'white'}
                    fontSize={24}
                    percentage={50}
                    strokeColorType={'image'}
                    strokeImage={gradientImage.src}
                    strokeWidth={8}
                    width={100}
                />

                <div className={style.tasksInfo_title}>
                    <Typography color={'white'} component={'h2'} variant={'body2'} weight={'bold'}>
                        Task
                        <br />
                        Completed
                    </Typography>
                </div>

                <div className={style.tasksInfo_button}>
                    <Button
                        color={'secondary'}
                        size={'small'}
                        startIcon={<CalendarIcon color={'gray6'} height={'18px'} width={'18px'} />}
                    >
                        Mar 22
                    </Button>
                </div>
            </div>

            <div style={{ marginTop: '20px' }}>
                <MobileDayPicker activeDay={date} onChange={setDate} />
            </div>
        </div>
    )
}

export default Home
