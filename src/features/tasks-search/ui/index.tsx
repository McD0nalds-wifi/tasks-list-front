'use client'

import { ChangeEvent } from 'react'

import { useSearchTasksQuery } from '@/features/tasks-search/api'
import { useDebouncedState } from '@/shared/hooks'
import { SearchInput } from '@/shared/uikit'

export const TasksSearch = () => {
    const {
        state: searchValue,
        debouncedValue: debouncedSearchValue,
        setState: setSearchValue,
    } = useDebouncedState({ defaultValue: '', delay: 400 })

    useSearchTasksQuery({ keyword: debouncedSearchValue, limit: 1, offset: 1 })

    const handleSearchInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setSearchValue(event.target.value)
    }

    return <SearchInput onChange={handleSearchInputChange} placeholder={'Поиск задач'} value={searchValue} />
}
