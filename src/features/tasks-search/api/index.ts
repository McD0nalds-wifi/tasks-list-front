import { Task } from '@/entities/task'
import { appApi } from '@/shared/api'

interface ISearchTasksArgs {
    keyword: string
    limit: number
    offset: number
}

interface ISearchTasksResponse {
    hasMore: boolean
    tasks: Array<Task>
}

export const searchTasksApi = appApi.injectEndpoints({
    endpoints: (build) => ({
        searchTasks: build.query<ISearchTasksResponse, ISearchTasksArgs>({
            // async onQueryStarted(_, { queryFulfilled, dispatch }) {
            //     try {
            //         const { data } = await queryFulfilled
            //
            //         // TODO set data
            //     } catch {
            //         // TODO add error handler
            //     }
            // },
            query: (args) => ({
                method: 'GET',
                params: args,
                url: 'tasks',
            }),
        }),
    }),
})

export const { useSearchTasksQuery } = searchTasksApi
