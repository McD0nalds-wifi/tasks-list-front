import { clsx } from 'clsx'
import { format } from 'date-fns'

import { Typography } from '@/shared/uikit'

import style from './index.module.scss'
import gradientImage from '../../../../../public/ellipseGradient.png'

interface IDayItemProps {
    active: boolean
    date: Date
    onClick: (date: Date) => void
}

export const DayItem = ({ active, date, onClick }: IDayItemProps) => {
    const handleClick = () => {
        onClick(date)
    }

    return (
        <div
            className={clsx(style.dayItem, { [style.dayItem__active]: active })}
            data-active={active}
            onClick={handleClick}
            style={{ backgroundImage: active ? `url(${gradientImage.src})` : 'none' }}
        >
            <div className={style.dayItem_title}>
                <Typography color={'inherit'} component={'h3'} variant={'h2'} weight={'bold'}>
                    {format(date, 'dd')}
                </Typography>
            </div>

            <div className={style.dayItem_subtitle}>
                <Typography color={'inherit'} variant={'body2'}>
                    {format(date, 'EEE')}
                </Typography>
            </div>
        </div>
    )
}
