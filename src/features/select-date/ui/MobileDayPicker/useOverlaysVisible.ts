import { RefObject, useCallback, useEffect, useState } from 'react'

import { useEventListener } from '@/shared/hooks'

export const useOverlaysVisible = (contentRef: RefObject<HTMLDivElement>) => {
    const [leftOverlayVisible, setLeftOverlayVisible] = useState(false)
    const [rightOverlayVisible, setRightOverlayVisible] = useState(false)

    const updateOverlaysStates = useCallback(() => {
        if (!contentRef.current) {
            return
        }

        setLeftOverlayVisible(contentRef.current.scrollLeft > 0)
        setRightOverlayVisible(
            contentRef.current.scrollWidth - contentRef.current.offsetWidth !== contentRef.current.scrollLeft,
        )
    }, [contentRef])

    // Initialize states
    useEffect(() => {
        updateOverlaysStates()
    }, [updateOverlaysStates])

    useEventListener('resize', updateOverlaysStates)

    return { leftOverlayVisible, rightOverlayVisible, updateOverlaysStates }
}
