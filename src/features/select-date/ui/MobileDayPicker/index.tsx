'use client'

import { useEffect, useRef } from 'react'

import { getDate, getDaysInMonth, getMonth, getYear } from 'date-fns'

import { useOverlaysVisible } from '@/features/select-date/ui/MobileDayPicker/useOverlaysVisible'

import { DayItem } from './DayItem'
import style from './index.module.scss'
import { Overlay } from './Overlay'

interface IMobileDayPickerProps {
    activeDay: Date
    onChange: (day: Date) => void
}

export const MobileDayPicker = ({ activeDay, onChange }: IMobileDayPickerProps) => {
    const daysListRef = useRef<HTMLDivElement>(null)

    const { leftOverlayVisible, rightOverlayVisible, updateOverlaysStates } = useOverlaysVisible(daysListRef)

    const daysInMonth = getDaysInMonth(activeDay)
    const activeDayNumber = getDate(activeDay)

    const centerByActiveElement = () => {
        if (!daysListRef.current) {
            return
        }

        const activeElement = daysListRef.current.querySelector('[data-active="true"]') as HTMLDivElement | null

        if (!activeElement) {
            return
        }

        daysListRef.current.scrollTo({
            behavior: 'smooth',
            left: activeElement.offsetLeft + activeElement.offsetWidth / 2 - daysListRef.current.offsetWidth / 2,
        })
    }

    useEffect(() => {
        centerByActiveElement()
    }, [activeDayNumber])

    return (
        <div className={style.wrapper}>
            <Overlay direction={'left'} visible={leftOverlayVisible} />

            <div className={style.daysList} onScroll={updateOverlaysStates} ref={daysListRef}>
                {Array.from({ length: daysInMonth }).map((_, index) => (
                    <DayItem
                        active={activeDayNumber === index + 1}
                        date={new Date(getYear(activeDay), getMonth(activeDay), index + 1)}
                        key={index}
                        onClick={onChange}
                    />
                ))}
            </div>

            <Overlay direction={'right'} visible={rightOverlayVisible} />
        </div>
    )
}
