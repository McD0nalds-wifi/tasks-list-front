import type { Meta, StoryObj } from '@storybook/react'

import { DayItem } from './DayItem'
import { MobileDayPicker } from './index'

const meta: Meta<typeof MobileDayPicker> = {
    component: MobileDayPicker,
}

export default meta
type Story = StoryObj<typeof MobileDayPicker>

export const Playground: Story = {
    args: {
        activeDay: new Date(),
    },
}

export const Item = () => <DayItem active={false} date={new Date()} onClick={() => null} />
