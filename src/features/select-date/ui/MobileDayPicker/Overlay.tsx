import style from './index.module.scss'

interface IOverlayProps {
    direction: 'left' | 'right'
    visible: boolean
}

export const Overlay = ({ direction, visible }: IOverlayProps) => (
    <div
        className={style.overlay}
        style={{
            left: direction === 'left' ? -30 : 'auto',
            opacity: visible ? 1 : 0,
            right: direction === 'right' ? -30 : 'auto',
            transform: direction === 'left' ? 'scaleX(-1)' : 'none',
        }}
    />
)
