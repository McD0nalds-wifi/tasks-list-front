'use client'

import { useGetUserQuery } from '@/entities/user'
import { Avatar, Typography } from '@/shared/uikit'

import style from './index.module.scss'

export const Header = () => {
    const { data: user } = useGetUserQuery()

    return (
        <div className={style.wrapper}>
            <Avatar size={'54px'} />

            <div className={style.welcomeBlock}>
                {user?.firstName && (
                    <Typography color={'white'} variant={'body2'}>
                        Welcome {user.firstName}
                    </Typography>
                )}

                <Typography color={'white'} variant={'h3'} weight={'bold'}>
                    Explore Tasks
                </Typography>
            </div>
        </div>
    )
}
