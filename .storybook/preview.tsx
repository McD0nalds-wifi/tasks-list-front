import type { Preview } from '@storybook/react'
import { Plus_Jakarta_Sans } from 'next/font/google'
import './global.css'

const plusJakartaSans = Plus_Jakarta_Sans({ subsets: ['latin'], weight: ['400', '500', '700'] })

const preview: Preview = {
    parameters: {
        actions: { argTypesRegex: '^on[A-Z].*' },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/i,
            },
        },
    },
    decorators: [
        (Story) => (
            <main className={plusJakartaSans.className}>
                <Story />
            </main>
        ),
    ],
}

export default preview
